const handlebars = require("express-handlebars");
const path = require("path");
const express = require("express");
const morgan = require("morgan");
const app = express();
const route = require("./routes");
const port = 8089;

app.use(morgan("combined"));

// express.static: đối với dạng là file tĩnh, nó sẽ kiểm tra thư mục mà ta cung cấp cho nó trong phương thức static này
app.use(express.static(path.join(__dirname, "public")));

// Trong engine có thể sử dụng extname để có thể đặt tên file handlebars ngắn hơn
app.engine(
  "hbs",
  handlebars.engine({
    extname: ".hbs",
  })
);
app.set("view engine", ".hbs");
// app.set("views", "./src/resources/views");
app.set("views", path.join(__dirname, "resources/views"));

// Routes init
route(app);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
  console.log(__dirname);
});
