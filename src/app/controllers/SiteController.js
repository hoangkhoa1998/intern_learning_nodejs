class SiteController {
  // [GET] / home
  home(req, res) {
    res.render("home");
  }

  //   [GET] / search
  search(req, res) {
    res.render("search");
  }

  blog(req, res) {
    res.render("blog");
  }
}

module.exports = new SiteController();
