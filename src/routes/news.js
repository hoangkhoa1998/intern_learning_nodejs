const express = require("express");
const router = express.Router();
const newsController = require("../app/controllers/NewsController");

// newsController.index

// Luôn để các route có biến id ở trên
router.use("/:id", newsController.show);
router.use("/", newsController.index);

module.exports = router;
